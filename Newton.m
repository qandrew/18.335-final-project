% Andrew Xia
% Wonseok Shin
% 18.335 Numerical Methods Final Project
% May 7 2016

% This method generates the Newton's Method

% References: 
% http://www.seas.ucla.edu/~vandenbe/103/lectures/unconstrained.pdf 13-15

% given initial x, tolerance eps > 0
% repeat
% 1. evaluate ?g(x) and ?2g(x). 
% 2. if k?g(x)k ?eps, return x. 
% 3. Solve ?2g(x)v= ??g(x). 
% 4.x :=x+v. 
% until maximum number of iterations is exceeded

% example: Newton('3*x^2 - 12', x, 5, 'convex')
% example: Newton('x^2 + y*x + y^2 - 12',[x;y], [5;5], 'convex')

function [x_sol,ex,all_x] = Newton(f,x_vars, x_0, function_name, tol, max_iter,alpha)
% f is the input function
% x_vars are the x_i that we input into the function
% function_name is the specific newton method that we will use
% x_0 is our initial starting location
% tol is our tolerance from the solution
% max_iter is the maximum number of iterations that we will allow

% x_sol is our solution output
% ex is our error per round

    if nargin == 4
        tol = 1e-4;
        max_iter = 10;
        alpha = .5;
    elseif nargin == 5
        max_iter = 10;
        alpha = .5;
    elseif nargin == 6
        alpha = .5;
    elseif nargin ~= 7
        error('newton: invalid input parameters');
    end
    
    switch function_name
        case 'convex'
            [x_sol,ex,all_x] = convex_Newton(f,x_vars, x_0, tol, max_iter);
        case 'nonconvex'
            [x_sol,ex,all_x] = nonconvex_Newton(f,x_vars, x_0, tol, max_iter,alpha);
        otherwise
            out = 0;
            disp('newton: please give valid function name')
    end
end


function [x_sol,ex,all_x] = convex_Newton(f,x_vars, x_0, tol, max_iter)
% f is the input function
% x_vars are the x_i that we input into the function
% x_0 is our initial starting location
% tol is our tolerance from the solution
% max_iter is the maximum number of iterations that we will allow

% x_sol is our solution output
% ex is our error per round

f = sym(f); %convert string to actual function
f_grad = gradient(f,x_vars); %gradient of fct
f_hess = hessian(f,x_vars); %hessian of fct
%if isAlways(eig(f_hess) > 0) == 0 %having variable problems
%    error('newton: hessian not positive definite');
%end
eval_grad = subs(f_grad,x_vars,x_0); %evaluate nabla_g(x_0)
eval_hess = subs(f_hess,x_vars,x_0);
all_x = zeros(size(x_0,1),max_iter);
all_x(:,1) = x_0;

ex(1) = norm(eval_grad); 

k = 2;
while (ex(k-1) >= tol) && (k <= max_iter) %line 2
    k;
    v = inv(eval_hess)*eval_grad; %line 3
    x_0 = x_0 - v; %line 4
    all_x(:,k) = x_0;
    eval_grad = subs(f_grad,x_vars,x_0); %evaluate nabla_g(x_0)
    eval_hess = subs(f_hess,x_vars,x_0); %line 1 of code
    ex(k) = norm(eval_grad);
    k = k+1; %the round incrementer
end
k
all_x = all_x(:,1:k-1);
x_sol = x_0;

end


function [x_sol,ex,all_x] = nonconvex_Newton(f,x_vars, x_0, tol, max_iter,alpha)
% this method allows for nonconvex calls
% f is the input function
% x_vars are the x_i that we input into the function
% x_0 is our initial starting location
% tol is our tolerance from the solution
% max_iter is the maximum number of iterations that we will allow
% alph is a parameter (0,.5,1)

% x_sol is our solution output
% ex is our error per round

f = sym(f); %convert string to actual function
f_grad = gradient(f,x_vars) %gradient of fct
f_hess = hessian(f,x_vars) %hessian of fct
if all(eig(f_hess) > 0) == 0
    not_posdef = 1;
else
    not_posdef = 0;
end
eval_grad = subs(f_grad,x_vars,x_0) %evaluate nabla_g(x_0)
eval_hess = subs(f_hess,x_vars,x_0)

ex(1) = norm(eval_grad); 

k = 2;
while (ex(k-1) >= tol) && (k <= max_iter) %line 2
    k
    if not_posdef == 1
        v = -eval_grad %line 3
    else
        v = -inv(eval_hess)*eval_grad %line 3
    end
    t = 1;
    % v = transpose(v);
    while subs(f,x_vars,t*v) > subs(f,x_vars,x_0) + alpha*t*eval_grad*v
       t = t/2; %line 4
    end
    x_0 = x_0 + t*v %line 4
    eval_grad = subs(f_grad,x_vars,x_0) %evaluate nabla_g(x_0)
    eval_hess = subs(f_hess,x_vars,x_0) %line 1 of code
    ex(k) = norm(eval_grad);
    k = k+1; %the round incrementer
end
k
all_x = all_x(:,1:k-1)
all_x
x_sol = x_0;

end