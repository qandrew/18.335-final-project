% Andrew Xia
% Wonseok Shin
% 18.335 Numerical Methods Final Project
% May 5 2016

% This method generates random data
% example: out = Data(3, 5,'random',0);


function out = Data(num_dim,num_data,function_name,ploting)
    switch function_name
        case 'random'
            out = Data_random(num_data,num_dim);
        case 'quad rand noise'
            f = @(x) x^2 + 3*x + 3;
            out = zeros(num_data,1);
            for i=1:num_data
                index = i*.1 - 30.1;
               out(i) = f(index) +  rand()*2 - 0.5;
            end
        case 'rand noise'
            out = zeros(num_data,1);
            for i=1:num_data
                index = i*.1 - 30.1;
               out(i) = rand()*abs(index) + rand();
            end
        otherwise
            out = 0;
            disp('ERROR: please give valid function name')
    end
    
    if ploting == 1
        scatter(out(1,:),out(2,:)),title('huding')
    end
            
end


function y = Data_random(num_dim,num_data)
y = rand(num_dim,num_data); % we want num_data cols, and num_dim rows
end


%% Useful References
% http://www.mathworks.com/help/curvefit/least-squares-fitting.html