% Andrew Xia
% Wonseok Shin
% 18.335 Numerical Methods Final Project
% May 7 2016

% This method calls the Newton's methods etc
% to run nonlinear least squares

% References
% http://www.mathworks.com/help/curvefit/least-squares-fitting.html
% http://www.seas.ucla.edu/~vandenbe/103/lectures/nlls.pdf

function [x,resid] = LeastSquares(A,y,function_name)
    x_vars = sym('x',[size(A,2) 1]);
    switch function_name
        case 'newton'
            disp('entering newton')
            R = A*x_vars - y; 
            f = norm(R)^2; %see UCLA slide 14-8
            [x,ex,all_x] = Newton(f,x_vars, zeros(size(x_vars)), 'convex');
            %plot(A(:,2),A*x,'k')
            % init x_0 to all 0s
        case 'richardson'
            disp('entering richardson')
            [x,error] = Richardson(A,y); %,0.00001,1000000);
            %plot(A(:,2),A*x,'r')
        case 'nedler mead'
            disp('entering nelder mead')
            R = A*x_vars - y; 
            f = norm(R)^2; %see UCLA slide 14-8
            [x,error,tot_error] = NedlerMead(f,x_vars);
            %plot(A(:,2),A*x,'g')
            
        case 'matlab'
            x = lsqr(A,y); %Matlab Default
        otherwise
            x = A\y; %dumb backsolve
    end
    resid = norm(A*x - y);
    scatter(A(:,2),y); hold on; %don't plot yet
    %line(A(:,2),A*x,'g')
end

% single variable linear example using backsolve:
% x = [0;1;2;3];
% A = [x.^0 x];
% y = [2;3;4;6];
% [huding,resid] = LeastSquares(A,y,'eww')

% single variable quad example using Newton:
% x = [0;1;2;3];
% A = [x.^0 x x.^2];
% y = [2;3;4;6];
% huding = LeastSquares(A,y,'newton')


% huding = LeastSquares(huA,huy,'eww')
% xdata = (0:0.1:2*pi)';
% y0 = sin(xdata);
% gnoise = y0.*(randn(size(y0))/10);
% y1 = y0 + gnoise;

%plot (xdata,y1)
%plot (xdata,y0)

% f = fittype('a*sin(b*x)');
% [fit1,gof,fitinfo] = fit(xdata,y1,f,'StartPoint',[1 1]);

