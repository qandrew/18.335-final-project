% Andrew Xia
% Wonseok Shin
% 18.335 Numerical Methods Final Project
% May 5 2016

% Step 1: generate data
num_dim = 1;
num_data = 10;
method = 'quad noise';
ploting = 0;

%% TEST 1: absolute fitting possible
x = 1:1:10;
A = fliplr(vander(x));
A = A(:,1:3); %we want quad regression
y = A*[1;4;3]; %y = 1 + 4x + 3x^2
%1y(7) = 12;
%tic
%[ned_x_sol,ned_resid] = LeastSquares(A,y,'nedler mead');
%toc
%tic
%[new_x_sol,new_resid] = LeastSquares(A,y,'newton');
%toc
% tic
% [rich_x_sol,rich_resid] = LeastSquares(A,y,'richardson')
% toc
%plot(A(:,2),A*ned_x_sol,'g'); hold on;
%plot(A(:,2),A*new_x_sol,'k'); %hold on;
%plot(A(:,2),A*rich_x_sol,'r');

%% TEST 2: single variable quad example with noise
% x = transpose(-30:.1:29.9);%Data(num_dim, num_data,method,ploting); 
% A = [x.^0 x x.^2];
% y = Data(1, 600,'quad rand noise',0);
% tic
% [qr_x_sol,qr_resid] = LeastSquares(A,y,'qr')
% toc
% tic
% [mat_x_sol,mat_resid] = LeastSquares(A,y,'matlab')
% toc
% tic
% [ned_x_sol,ned_resid] = LeastSquares(A,y,'nedler mead')
% toc
% tic
% [new_x_sol,new_resid] = LeastSquares(A,y,'newton')
% toc
% tic
% [rich_x_sol,rich_resid] = LeastSquares(A,y,'richardson')
% toc
% plot(A(:,2),A*ned_x_sol,'g'); hold on;
% plot(A(:,2),A*new_x_sol,'k'); %hold on;
% plot(A(:,2),A*rich_x_sol,'r');

%% TEST 3: linear regression
x = transpose(-30:.1:29.9);%Data(num_dim, num_data,method,ploting); 
A = [x.^0 x x.^2];
y = Data(1, 600,'rand noise',0);
tic
[ned_x_sol,ned_resid] = LeastSquares(A,y,'nedler mead')
toc
tic
[new_x_sol,new_resid] = LeastSquares(A,y,'newton')
toc
tic
[rich_x_sol,rich_resid] = LeastSquares(A,y,'richardson')
toc
plot(A(:,2),A*ned_x_sol,'g'); hold on;
plot(A(:,2),A*new_x_sol,'k'); %hold on;
plot(A(:,2),A*rich_x_sol,'r');



