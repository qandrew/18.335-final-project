% Andrew Xia
% Wonseok Shin
% 18.335 Numerical Methods Final Project
% May 5 2016

% This method should implement the richardson iteration

% references
% http://ta.twi.tudelft.nl/nw/users/gijzen/CURSUS_DTU/LES1/TRANSPARANTEN/les1.pdf
% https://www.wias-berlin.de/people/john/LEHRE/NUMERIK_II/linsys_4.pdf


function [x_sol, ex] = Richardson(A,b,weight,max_iter)
    if nargin == 2
       if size(A,1) ~= size(A,2) %is square 
           b = A.'*b;
           A = A.'*A;
       end
       eigs = eig(A)
       if eigs(1) >= 0
           weight = 2/(eigs(1) + eigs(size(A,1))) %max and min eig
           [x_sol, ex] = Richardson_square(A,b,weight,ceil(1/weight));
       end
    else
        if size(A,1) == size(A,2) %is square
           [x_sol, ex] = Richardson_square(A,b,weight,max_iter);
        else
            [x_sol, ex] = Richardson_rect(A,b,weight,max_iter);
        end
    end
end

function [x_sol, ex] = Richardson_square(A,b,weight,max_iter)
    tol = 0.0001;
    x = zeros(size(b,1),max_iter+1); %matrix to keep track of all generated x
    error = zeros(size(b,1),max_iter+1);
    k = 1;
    x(:,k) = b;
    error(:,k) = b; %random value
    while norm(error(:,k)) > tol && k < max_iter
        k = k+1;
        x(:,k) = x(:,k-1) + weight*(b - A*x(:,k-1));
        error(:,k) = error(:,k-1) - weight*A*error(:,k-1);
    end
    k
    x_sol = x(:,k-1);
    ex = error(:,k-1);


end

function [x_sol, ex] = Richardson_rect(A,b,weight,max_iter)
    tol = 0.0001;
    new_A = A.'*A %make square
    new_b = A.'*b %make correct dim
    x = zeros(size(new_b,1),max_iter+1); %matrix to keep track of all generated x
    error = zeros(size(new_b,1),max_iter+1);
    k = 1;
    x(:,k) = new_b;
    error(:,k) = new_b; %random value
    while norm(error(:,k)) > tol && k < max_iter
        k = k+1;
        x(:,k) = x(:,k-1) + weight*(new_b - new_A*x(:,k-1));
        error(:,k) = error(:,k-1) - weight*new_A*error(:,k-1);
    end
    k
    x_sol = x(:,k-1)
    ex = error(:,k-1)
end
