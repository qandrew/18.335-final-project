% Andrew Xia
% Wonseok Shin
% 18.335 Numerical Methods Final Project
% May 5 2016

% This method deals with the Nedler Mead Algorithm

% References
% http://www.personal.soton.ac.uk/rchc/Teaching/GTPBootstrap/ExtraDetails/NelderMeadProof.pdf
% https://github.com/fchollet/nelder-mead/blob/master/nelder_mead.py
% https://en.wikipedia.org/wiki/Nelder%E2%80%93Mead_method

function [x_sol,ex,tot_error] = NedlerMead(f,x_vars, x_0, tol, max_iter,alpha,gamma,rho,sigma)
    %f is our input function
    %x_vars. we want a horizontal format
    %x_0 is our starting point. we want a horiz format
    %tol is our tolerance threshold, which we'll tolerate for
    %no_improv_thresh rounds
    %max_iter is the maximum number of iterations allowed
    %alpha is the reflection coefficient, defaulted to 1
    %gamma is the expansion coefficient, defaulted to 2
    %rho is the contraction coefficient, defaulted to -1/2
    %sigma is the shirnk coefficient, defaulted to 1/2
    
    step = 0.2; %the initial step size
    no_improv_thresh = 10;
    
    if nargin == 2 %we only give f and x_vars
        x_0 = zeros(size(x_vars));
        tol = 10e-6;
        max_iter = 1000;
        alpha = 1;
        gamma = 2;
        rho = -0.5;
        sigma = 0.5;
    elseif nargin == 5
        alpha = 1;
        gamma = 2;
        rho = -0.5;
        sigma = 0.5;
    elseif nargin > 9
        error('newton: invalid input parameters');
    end
    
    [~,index_of] = max(size(x_vars));
    if index_of == 1 %run a transpose to make horiz vector
        x_vars = transpose(x_vars);
        x_0 = transpose(x_0);
    end
    
    tot_error = zeros(max_iter);
    
    dim = max(size(x_vars));
    no_improv = 0;
    res = zeros(dim+1,dim+1); %first dim cols are x input, dim+1 col is f(x)
    res(1,1:dim) = x_0;
    prev_best = subs(f,x_vars,x_0); %prev_best
    res(1,dim+1) = prev_best;
    
    for i=1:dim
        res(i+1,1:dim) = x_0;
        res(i+1,i) = res(i+1,i) + step;
        res(i+1,dim+1) = subs(f,x_vars,res(i+1,1:dim));
    end
    
    iters = 0;
    while 1 == 1 %continue forever
        iters = iters + 1;
        %1. order all data
        res = sortrows(res,dim+1); %sort by function value
        tot_error(iters) = res(1,dim+1);
        
        %disp('best so far')
        %disp(res(1,dim+1)) %best
        
        %break after no improv or max data
        if iters >= max_iter
           iters
           %disp('reached max iter')
           x_sol = transpose(res(1,1:dim)); %take the best value. output vertical
           ex = res(1,dim+1); %optimal output value
           tot_error = tot_error(1:iters);
           break
        end
        if res(1,dim+1) < prev_best - tol
            no_improv = 0;
            prev_best = res(1,dim+1);
        else
            no_improv = no_improv + 1; %count number of rounds without improve
        end
        if no_improv >= no_improv_thresh
            %disp('no improve break')
            iters
            tot_error = tot_error(1:iters);
            x_sol = transpose(res(1,1:dim)); %take the best value. output vertical
            ex = res(1,dim+1); %optimal output value
            break
        end
        
        %2. calculate centroid
        x_cent = zeros(size(x_0));
        for i=1:dim %skip worst value
            x_cent = x_cent + res(i,1:dim)/dim;
        end
        
        %3. reflection
        xr = x_cent + alpha*(x_cent - res(dim+1,1:dim)); %worst val
        rscore = subs(f,x_vars,xr);
        if dim ==1
            if res(1,dim+1) <= rscore && rscore < res(1,dim+1)
                res(dim+1,:) = [xr rscore]; %obtain a new simplex
                %disp('reflection')
                continue
            end
        else
            if res(1,dim+1) <= rscore && rscore < res(dim-1,dim+1)
                res(dim+1,:) = [xr rscore]; %obtain a new simplex
                %disp('reflection')
                continue
            end
        end
        
        %4. expansion
        if rscore < res(1,dim+1) %best score
            %disp('rscore < res')
            xe = x_cent + gamma*(x_cent - res(dim+1,1:dim)); %worst val
            escore = subs(f,x_vars,xe);
            if escore < rscore
                res(dim+1,:) = [xe escore]; %obtain a new simplex
                %disp('expansion xe')
                continue %to next iteration
            else
                res(dim+1,:) = [xr rscore]; %obtain a new simplex
                %disp('expansion xr')
                continue %to next iteration
            end
        end
        
        %5. contraction
        xc = x_cent + rho*(x_cent - res(dim+1,1:dim)); %worst val
        cscore = subs(f,x_vars,xc);
        
        if cscore < res(dim+1,dim+1) %better than worst val
            res(dim+1,:) = [xc cscore]; %obtain a new simplex
            %disp('contraction')
            continue %to next iteration            
        end
        
        %6. shrink
        x1 = res(1,1:dim);
        for i=2:dim+1
            reduced_x = x1 + sigma*(res(i,1:dim) - x1);
            res(i,1:dim) = reduced_x;
            res(i,dim+1) = subs(f,x_vars,reduced_x); %calculate new score
        end
        %disp('shrink')
        
    end
  


end